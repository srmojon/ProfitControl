from flask_appbuilder import ModelView, IndexView, expose
from flask import redirect
from flask_login import current_user
from app.info_server import InfoServer
from app.service_manager import SystemD


class IndexView(IndexView):

    route_base = '/'
    default_view = 'index'
    index_template = 'index.html'
    vps = InfoServer()
    service = SystemD()

    @expose('/')
    def index(self):
        self.update_redirect()
        if current_user.is_authenticated():
            estados = {}
            estados['profit'] = self.service.shortStatus('profit')
            estados['magic'] = self.service.shortStatus('magic')
            estados['monitor'] = self.service.shortStatus('monitor')
            # obtenemos los datos del vps
            info = self.vps.getHTML()
            return self.render_template(self.index_template, info=info, appbuilder=self.appbuilder, estados=estados)
        else:
            return redirect('/login')
