from subprocess import check_output
import os


class SystemD:

    """docstring for ClassName"""

    def isActive(self, servicio):
        cmd = 'sudo systemctl is-active %s.service' % servicio
        result = comando(cmd)
        if result.strip() == "active":
            return True
        else:
            return False

    def isFailed(self, servicio):
        cmd = 'sudo systemctl is-failed %s.service' % servicio
        result = comando(cmd)
        if result == "failed":
            return True
        else:
            return False

    def status(self, servicio):
        cmd = 'sudo systemctl status %s.service' % servicio
        result = comando(cmd)
        return result

    def start(self, servicio):
        cmd = 'sudo systemctl start %s.service' % servicio
        result = comando(cmd)
        return result

    def stop(self, servicio):
        cmd = 'sudo systemctl stop %s.service' % servicio
        result = comando(cmd)
        return result

    def restart(self, servicio):
        cmd = 'sudo systemctl restart %s.service' % servicio
        result = comando(cmd)
        return result

    def shortStatus(self, servicio):
        if self.isActive(servicio):
            res = "Activado"
        else:
            res = "Desactivado"
        return res


# ejecuta comando bash y devuelve la respuesta
def comando(cmd):
    try:
        lista = cmd.split(" ")
        out = os.popen(cmd)
        r = str(out.read())
        return r
    except Exception as e:
        raise e
        return "error"
