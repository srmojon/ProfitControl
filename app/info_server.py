import platform as plat
import psutil
import humanize


class InfoServer:

    """Clase Info

    Obtiene datos acerca de la maquina servidor (ram, cpu, etc)
    Guarda todos los datos en un diccionario
    """

    def __init__(self):
        # obtiene los datos estaticos
        lista = {}
        lista['arch'] = plat.architecture()[0]
        lista['py_version'] = plat.python_version()
        lista['os'] = plat.system()
        lista['ram'] = {}
        lista['cpu'] = {}
        self.lista = lista

    def updateSys(self):
        # obtiene los datos dinamicos
        ram = psutil.virtual_memory()
        self.lista['ram']['percent'] = ram.percent
        self.lista['ram']['used'] = humanize.naturalsize(ram.used)
        self.lista['cpu']['cores'] = psutil.cpu_count()
        self.lista['cpu']['percent'] = psutil.cpu_percent(interval=1)
        self.lista['uptime'] = humanize.naturaltime(psutil.boot_time())

    def getHTML(self):
        self.updateSys()
        s = "OS: %s | " % self.lista['os']
        s += "CPUs: %s | CPU en uso: %s | " % (self.lista['cpu']['cores'], self.lista['cpu']['percent'])
        s += "RAM en uso: %s" % self.lista['ram']['used']
        return s
