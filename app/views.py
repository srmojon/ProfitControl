from flask import render_template, redirect
from flask_appbuilder.models.sqla.interface import SQLAInterface
from flask_appbuilder import BaseView, expose, has_access
from app import appbuilder, db
from app.service_manager import SystemD


class ProfitView(BaseView):

    route_base = "/profit"
    default_view = "start"
    sysd = SystemD()

    @expose('/<string:comando>')
    @has_access
    def start(self, comando):
        print(comando)
        if comando == "start":
            self.sysd.start('profit')
        elif comando == "stop":
            self.sysd.stop('profit')
        elif comando == "reset":
            self.sysd.restart('profit')
        else:
            return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder)
        return redirect('/')


class MagicView(BaseView):

    route_base = "/magic"
    default_view = "start"
    sysd = SystemD()

    @expose('/<string:comando>')
    @has_access
    def start(self, comando):
        print(comando)
        if comando == "start":
            self.sysd.start('magic')
        elif comando == "stop":
            self.sysd.stop('magic')
        elif comando == "reset":
            self.sysd.restart('magic')
        else:
            return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder)
        return redirect('/')


class MagicMonitorView(BaseView):

    route_base = "/monitor"
    default_view = "start"
    sysd = SystemD()

    @expose('/<string:comando>')
    @has_access
    def start(self, comando):
        print(comando)
        if comando == "start":
            self.sysd.start('monitor')
        elif comando == "stop":
            self.sysd.stop('monitor')
        elif comando == "reset":
            self.sysd.restart('monitor')
        else:
            return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder)
        return redirect('/')


"""
    Application wide 404 error handler
"""


@appbuilder.app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder), 404
    #     return render_template('404.html', base_template=appbuilder.base_template, appbuilder=appbuilder), 404


db.create_all()
appbuilder.add_view_no_menu(ProfitView)
appbuilder.add_view_no_menu(MagicView)
appbuilder.add_view_no_menu(MagicMonitorView)
